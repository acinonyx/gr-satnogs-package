cmake_minimum_required (VERSION 2.8.11 FATAL_ERROR)
project (gr-satnogs-package NONE)

file(
  COPY
  debian
  build.sh
  Dockerfile.amd64
  Dockerfile.armhf
  Dockerfile.arm64
  docker-compose.yml
  DESTINATION .
  )

add_custom_target(
  build-amd64
  COMMAND mkdir -p output/amd64
  COMMAND docker-compose build --pull --force-rm debian-amd64
  COMMAND docker-compose run -T --rm debian-amd64 /usr/local/bin/build.sh "${VERSION}"
  )

add_custom_target(
  build-armhf
  COMMAND mkdir -p output/armhf
  COMMAND docker run --rm --privileged multiarch/qemu-user-static:register --reset
  COMMAND docker-compose build --pull --force-rm debian-armhf
  COMMAND docker-compose run -T --rm debian-armhf /usr/local/bin/build.sh "${VERSION}"
  )
add_custom_target(
  build-arm64
  COMMAND mkdir -p output/arm64
  COMMAND docker run --rm --privileged multiarch/qemu-user-static:register --reset
  COMMAND docker-compose build --pull --force-rm debian-arm64
  COMMAND docker-compose run -T --rm debian-arm64 /usr/local/bin/build.sh "${VERSION}"
  )
add_custom_target(
  build ALL
  )

add_dependencies(
  build
  build-amd64
  build-armhf
  build-arm64
  )
