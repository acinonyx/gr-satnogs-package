#!/bin/sh

DEBIAN_DIR="/tmp/debian"
WORKING_DIR="/tmp/dpkg"
PACKAGE_NAME="gr-satnogs"

# Change to working directory
cd "$WORKING_DIR"

# Create package directory
mkdir -p "$WORKING_DIR"/"$PACKAGE_NAME"

# Copy debian packaging directory
cp -R "$DEBIAN_DIR" "$WORKING_DIR"/"$PACKAGE_NAME"/

# Update changelog
if [ -n "$1" ]; then
	cd "$WORKING_DIR"/"$PACKAGE_NAME"
	dch -b -M -v "$1" "Bump to version $1"
	dch -r -m ""
fi

# Get original source code
cd "$WORKING_DIR"
"$PACKAGE_NAME"/debian/rules get-orig-source

# Untar original source code
tar -C "$WORKING_DIR"/"$PACKAGE_NAME" --strip-components=1 -xvf "$WORKING_DIR"/"$PACKAGE_NAME"*.orig.tar.xz

# Build package
cd "$WORKING_DIR"/"$PACKAGE_NAME"
dpkg-buildpackage -us -uc -j$(nproc)

# Fix ownership of files
chown -R "$(stat -c "%u:%g" "$WORKING_DIR")" "$WORKING_DIR"
